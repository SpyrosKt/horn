
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {
	
	private static KnowledgeBase KB = new KnowledgeBase();
	private static Type typeToProve;
	
	public static void main(String[] args) {
		
		//readFile();		// C:\Users\Spyros\IdeaProjects\Horn\resources\example.txt
		
		//readType();
		
		//System.out.println(KB.entails(typeToProve));
		
		System.out.println('¬' + 1);
		System.out.println('∨' + 1);
		System.out.println('∧' + 1);
		System.out.println('→' + 1);
		System.out.println('↔' + 1);
	}
	
	private static void readFile() {
		
		Scanner s = new Scanner(System.in);
		System.out.println("File path: ");
		String pathfile = s.nextLine();
		
		while(true)
			try {
				KB = new KnowledgeBase(pathfile);
				break;
			}
			catch (Exception e) {
				System.out.println("Knowledge base could not be created from " + pathfile);
				System.out.println("File path: ");
				pathfile = s.nextLine();
			}
			
	}
	
	private static void readType() {
		
	}
	
	private static Type toType(String s) {
		
		int parentheses = 0;
		int minParentheses = Integer.MAX_VALUE;
		int operatorIndex = -1;
		ArrayList<Integer> operatorIndices = new ArrayList();	// in case of multiple ∨'s or ∧'s
		
		for(int i = 0; i < s.length(); i++) {
			if(s.charAt(i) == '(')
				parentheses++;
			else if (s.charAt(i) == ')')
				parentheses--;
			else if(s.charAt(i) == '¬' || s.charAt(i) == '∨' || s.charAt(i) == '∧' || s.charAt(i) == '→' || s.charAt(i) == '↔') {
				if(parentheses < minParentheses) {
					operatorIndex = i;
					minParentheses = parentheses;
					operatorIndices.clear();
					if(s.charAt(i) == '∨' || s.charAt(i) == '∧')
						operatorIndices.add(new Integer(i));
				}
				// if more than one operators with the same "depth" are found, they need to be prioritized
				else if(parentheses == minParentheses) {
					if(s.charAt(i) == '→' || s.charAt(i) == '↔') {
						operatorIndex = i;
						minParentheses = parentheses;
					}
					else if(s.charAt(i) == '∨' || s.charAt(i) == '∧') {
						if(s.charAt(i) == s.charAt(operatorIndex))    // there are multiple ∨'s or ∧'s
							operatorIndices.add(new Integer(i));
						else if(s.charAt(operatorIndex) == '¬') {
							operatorIndex = i;
							minParentheses = parentheses;
						}
					}
				}
			}
		}
		
		
		if(operatorIndex == -1)		// no operator was found
			return new Literal(s.replace("(", "").replace(")", "").replace(" ", ""));
		char operator = s.charAt(operatorIndex);
		
		
		ArrayList<Type> operands = new ArrayList<Type>();
		
		
		
		switch(operator) {
			case '¬':
				return new Negation(toType(s.substring(operatorIndex + 1)));
			case '∨':
				return new Disjunction(operands);
				// OR return new Disjunction(toType(s.substring(0, operatorIndex - 1)), toType(s.substring(operatorIndex + 1)));
			case '∧':
				return new Conjunction(operands);
				// OR return new Conjunction(toType(s.substring(0, operatorIndex - 1)), toType(s.substring(operatorIndex + 1)));
			case '→':
				return new Conditional(toType(s.substring(0, operatorIndex - 1)), toType(s.substring(operatorIndex + 1)));
			default:	//case '↔':
				return new Biconditional(toType(s.substring(0, operatorIndex - 1)), toType(s.substring(operatorIndex + 1)));
		}		
	}
	
}
