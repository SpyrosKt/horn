
import java.util.ArrayList;
import java.util.StringTokenizer;

public class HornClause {
	
	private ArrayList<Literal> negatedLiterals = new ArrayList<Literal>();
	private Literal fact;
	
	public HornClause(String s) {
		StringTokenizer tokenized = new StringTokenizer(s, "( ∨ )");
		Literal currentLiteral;
		while (tokenized.hasMoreElements()) {
			currentLiteral = new Literal(tokenized.nextToken());
			if(currentLiteral.isNegated())				
				negatedLiterals.add(currentLiteral);
			else
				fact = currentLiteral;
		}
	}
	
	public ArrayList<Literal> getNegatedLiterals() {
		return negatedLiterals;
	}
	
	// returns the literals that are negated after "turning" them to positive
	public ArrayList<Literal> getLiterals() {
		ArrayList<Literal> a = new ArrayList<Literal>();
		for(Literal l:negatedLiterals)
			a.add(l.getNegation());
		return a;
	}
	
	public Literal getFact() {
		return fact;
	}
	
	public boolean containsAsPremise(Literal lit) {
		ArrayList<Literal> literals = getLiterals();
		for(Literal l:literals)
			if(l.equals(lit))
				return true;
		return false;
	}
	
	public boolean equals(HornClause h) {
		if(h.toString().equals(this.toString()))
			return true;
		return false;
	}
	
	public int amountOfPremises() {
		return negatedLiterals.size();
	}
	
	public String toString() {
		String s = "(";
		for(Literal l:negatedLiterals)
			s += l.toString() + " ∨ ";
		s += fact.toString() + ")";
		return s;
	}
	
	public String toStringAlt() {
		String s = "(";
		ArrayList<Literal> literals = getLiterals();
		for(int i = 0; i < literals.size(); i++)
			if(i != literals.size() - 1)
				s += literals.get(i).toString() + " ∧ ";
			else
				s += literals.get(i).toString() + " → ";
		s += fact.toString() + ")";
		return s;
	}
	
	public boolean isLiteral() {
		return negatedLiterals.isEmpty();
	}
	
	public Literal toLiteral() {
		if(!negatedLiterals.isEmpty())
			return null;
		return fact;
	}	
	
}
