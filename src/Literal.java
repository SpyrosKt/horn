
public class Literal implements Type {
	
	private String name;
	private boolean negated;
	
	public Literal(String name) {
		if(name.startsWith("¬")) {
			this.name = name.substring(1);
			this.negated = true;
		}
		else {
			this.name = name;
			this.negated = false;
		}
	}
	
	public Literal(String name, boolean negated) {
		this.name = name;
		this.negated = negated;
	}
	
	public boolean isTrue(KnowledgeBase KB) {
		return KB.entails(this);
	}
	
	public boolean equals(Type t) {
		return t.toString().equals(this.toString());
	}
	
	public String toString() {
		if(negated)
			return "¬" + name ;
		return name;
	}
	
	public Literal getNegation() {
		return new Literal(name, !negated);
	}
	
	public boolean isNegated() {
		return negated;
	}
	
}
