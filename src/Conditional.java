
public class Conditional implements Type {
	
	Type leftOperand, rightOperand;
	
	public Conditional(Type leftOperand, Type rightOperand) {
		this.leftOperand = leftOperand;
		this.rightOperand = rightOperand;
	}
	
	public Conditional(String s) {
		
	}
	
	public boolean isTrue(KnowledgeBase KB) {
		if(!leftOperand.isTrue(KB)) return true;
		if(rightOperand.isTrue(KB)) return true;
		return false;
	}
	
	public boolean equals(Type t) {
		if(t.toString().equals(this.toString()))
			return true;
		return false;
	}
	
	public String toString() {
		return "(" + leftOperand.toString() + " → " + rightOperand.toString() + ")";
	}
	
}
