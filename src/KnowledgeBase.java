
import java.util.ArrayList;
import java.util.HashMap;
import java.io.BufferedReader;
import java.io.FileReader;


public class KnowledgeBase {

	private ArrayList<HornClause> clauses = new ArrayList<HornClause>();
	
	public KnowledgeBase(String pathfile) throws Exception {
		BufferedReader br = new BufferedReader(new FileReader(pathfile));
		String currentLine;
		while((currentLine = br.readLine()) != null)
			add(new HornClause(currentLine));
		
		br.close();
	}
	
	public KnowledgeBase() {
		
	}
	
	public boolean entails(Literal q) {
		
		HashMap<HornClause, Integer> premises = new HashMap<HornClause, Integer>();		// the amount of literals not satisfied for each clause
		ArrayList<Literal> inferredLiterals = new ArrayList<Literal>();
		ArrayList<Literal> agenda = new ArrayList<Literal>();
		
		for(HornClause h:clauses) {
			premises.put(h, new Integer(h.amountOfPremises()));
			if(h.isLiteral())
				agenda.add(h.toLiteral());
		}
		
		for(Literal l:agenda)
			if(l.equals(q))
				return true;
		
		while(!agenda.isEmpty()) {
			Literal currentlyInspected = agenda.remove(0);
			if(!inferredLiterals.contains(currentlyInspected)) {
				inferredLiterals.add(currentlyInspected);
				for(HornClause h:clauses) {
					if(h.containsAsPremise(currentlyInspected)) {
						int p = premises.get(h).intValue();
						premises.put(h, new Integer(p - 1));
						if(premises.get(h).intValue() == 0) {
							if(h.getFact().equals(q))
								return true;
							agenda.add(h.getFact());
						}
					}
				}
			}
		}
		return false;
	}
	
	public boolean entails(Type t) {
		return t.isTrue(this);
	}
	
	public void add(HornClause h) {
		clauses.add(h);
	}
	
	public boolean contains(HornClause c) {
		for(HornClause c2:clauses)
			if(c.equals(c2))
				return true;
		return false;
	}
	
	public String toString() {
		String s = "";
		int i = 1;
		for(HornClause h:clauses)
			s += i++ + ") " + h.toString() + "\n   " + h.toStringAlt() + "\n";
		return s;
	}
	
}
