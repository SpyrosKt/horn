
public class Negation implements Type {

	Type operand;
	
	public Negation(Type operand) {
		this.operand = operand;
	}
	
	public Negation(String s) {
		
	}
	
	public boolean isTrue(KnowledgeBase KB) {
		return !operand.isTrue(KB);
	}
	
	public boolean equals(Type t) {
		if(t.toString().equals(this.toString()))
			return true;
		return false;
	}
	
	public String toString() {
		return "¬" + operand.toString();
	}
	
}
