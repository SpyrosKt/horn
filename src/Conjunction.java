
import java.util.ArrayList;

public class Conjunction implements Type {
	
	private ArrayList<Type> operands = new ArrayList<Type>();
	
	public Conjunction(ArrayList<Type> operands) {
		this.operands = operands;
	}
	
	public Conjunction(String s) {
		
	}
	
	public boolean isTrue(KnowledgeBase KB) {
		for(Type t: operands)
			if(!t.isTrue(KB)) return false;
		return true;
	}
	
	public boolean equals(Type t) {
		if(t.toString().equals(this.toString()))
			return true;
		return false;
	}
	
	public String toString() {
		String s = "(";
		for(int i = 0; i < operands.size() - 1; i++)
			s += operands.get(i).toString() + " ∧ ";
		s += operands.get(operands.size() - 1).toString() + ")";
		return s;
	}
	
}
