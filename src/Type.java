
public interface Type {
	
	public boolean isTrue(KnowledgeBase KB);
	
	public boolean equals(Type t);
	
	public String toString();
	
}
